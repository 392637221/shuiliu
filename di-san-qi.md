# 第三期

### **第三期：你好，陌生人**

你按照节目组的安排来到约会地点，等在那里会是谁？你满怀忐忑和期待。

女生到达时，你转身的那一刻，看到的就是姜安心，那个带着茉莉花香的女孩。你笑着走向她，心知这是你的机会。

你对姜安心的感觉很不一样，你好像终于明白，有时候，喜欢一个人真的毫无理由，也没有办法，就像吴倩尹对你的喜欢一样。

感情这东西似乎是盏短暂的烛火，你靠近它汲取温暖，你与吴倩尹的光亮，太微弱太微小，你无法去依赖那样的光芒。

直到你在这个节目中遇见了姜安心。

她就像是一轮高悬的明月，柔和又璀璨，足以照亮你的生活与生命，即使是这样的没有缘由，可在看见她的第一眼你就有种感觉——她就是你所需要的“月光”。

即使你对她并不了解，但爱情，就是这么蛮横，谁爱谁，谁不爱谁，都没有道理可讲。

你遇到她，就如同还感情债一样，还的又是谁的债？你不敢细想。约会开始时已经临近傍晚，你带着姜安心去一家雅致的音乐餐厅吃饭。你跟她说，不是有句话么，唯音乐与美食不可辜负，当这二者结合在一起，身处其间，看着坐在对面的那个人，似乎也能多觉出几分浪漫。

姜安心的兴致其实不高，她像是藏着什么心事，情绪也有些低落，你能看出来。她神情恍惚，机械性的把手边布丁上所有的蓝莓都挑走了，原来她不喜欢蓝莓。

你暗暗记在心里，忽然顿了一下。原来，你也可以是一个细致的林沐阳，原来，你其实可以观察、记下别人的喜好。

吴倩尹，她不喜欢吃什么呢？不，这次约会不该胡思乱想，你和姜安心之间，不能是这样的气氛。

为了缓和气氛，你和她谈起星座，顺势问起了姜安心的生日。生日是个好东西，如果能问到她的生日，在她生日的当天送上一份用心的礼物，或许能拉近与她的关系。而此刻顺着生日的话题往下讲，也能涉及生活的各个方面，让你能在缓和气氛的同时更加了解姜安心。

姜安心淡淡地说她的生日已经过了，就在上节目的前几天。

你没想到她的生日已经过去，心里盘算的计划也只好打消。但话题还可以继续，你顺着问她生日那天做了些什么，想让她显得开心一点。

她的眼神却忽然暗了下来，沉默了一会儿，用最随意的语气说：“生日就独自一人在家拼乐高啊，又不是小女孩了，难道去迪士尼看烟花许愿啊。”

你愣住了，哪怕只是随着姜安心的话去想象，都能感受到在那个特殊的日子里，只有她一人在房间里拼接乐高时的孤独与寥落，那个再无他人的房间中的清冷与灰暗。

你看着她的眼睛，明明，那里面的落寞你捕捉到了，这样落寞的眼神，你分明在别的什么地方，也见到过——同样在生日时，一个人祝自己生日快乐的吴倩尹。那时的她，房间里也是同样的冷清吗？

一瞬间，莫名其妙的，你的心疼了起来，却不知道是为了眼前的姜安心，还是为了回忆里的吴倩尹。

姜安心似乎有些尴尬，低下头躲避你的目光，她解释说这很正常啊，女孩子一过二十五岁，每年最讨厌的就是过生日，好像不过生日，就可以青春永驻一样。

你的心中似乎有什么东西被点燃了，你觉得不该是这样，她不该拥有那样的一个生日，那样一个孤单的生日，她该有一个璀璨的绚烂的祝福，该有人把她的生日记忆从冷清替换成美好。

你猛地站了起来，把姜安心都吓了一跳，对她说：“走吧，我带你去，给你补一个有意义的生日！”你郑重地向她伸出了邀请的手，她犹豫了几秒，忽地欣然一笑，把手放在你的掌心。

你看着那双漂亮的眼睛，像是看见一抹霞光从未亮的黎明深处亮起，那样的明媚，那样的灿烂，你无比庆幸自己做出了这个决定。

你们离开了餐厅，打了车，你很坚定地说：“师傅，去迪士尼。”

那时，正值晚高峰，你们的车被堵在路上，不知道何时才能到达目的地。你有些焦急，不停地用手机确认时间，屏幕亮起又暗下，你的目光穿过车窗，一直看向车流尽头。

如果你没记错的话，迪士尼晚上八点会有一场烟花秀，一定没有女孩不喜欢烟火，烟火之下最适合庆祝生日。但如果你们一直被堵在路上，那场烟花秀会在你们到达前结束，你将无比遗憾。

与你相反，姜安心一直看着窗外，不急不躁。她的眼中倒映着远方的车流，灯火落在她的眸里，绚丽得令人神往，她的眼神却满是虚无。

你想或许过去那些独处的日子里，她也时常像这样望着远方，或许正是习惯了孤独的日子，她才如此淡然，这更让你坚定了带她去看烟花的决定，你要给她过上一个有意义的、有人陪伴的生日。

似乎是你的期盼引发了某种奇迹，一直停滞的车流开始移动。道路渐渐畅通，你们很快就到了迪士尼门口。

生日自然少不了生日蛋糕。

你让姜安心在原地等你，准备去就近的蛋糕店买生日蛋糕。离开前她似乎想要留你，但伸出了手却还是收回，放你离去。

等你到达蛋糕店时，店里只剩下了一份点缀着蓝莓的生日蛋糕。

刚才的饭桌上你有留意姜安心不喜欢吃蓝莓，但眼下已经没有时间再去订做一份新的了，只好将就。为了给她营造足够的惊喜，你让店员将蛋糕包装得平平无奇，甚至看不出里面装的是蛋糕。

回到姜安心身边时，你远远看到她望着远处，一副失神的样子。你到了她的身边她都没有发现，但你却已经发现了她眼中的羡慕与寂寥。顺着姜安心的目光望去，你看见乐园门口许多人走过，他们没有什么共同点，唯一的共同点就是他们都有人陪伴。

你唤回了走神的姜安心，带着她去到乐园门口，却得知现在是晚上7：40，已经不能再放人进去。

你想坚持，但姜安心听完却劝你说：“算了，还是别去了。”

你想起她望着乐园门口的人群时，眼中的羡慕与寂寥。你明白她是想起了过去自己孤单的日子，明白她也向往着有人能陪伴自己。

但今天她不必再羡慕别人，今天有你在她身边，必定不会再让她感到孤寂。

你要弥补她的生日，也弥补，你心中亏欠的那个生日。

你跟工作人员好说歹说，讲明理由，终于是把他们说通，同意放你们进去。

你迅速地买好了票，时间已经有些不够，你一把抓住姜安心的手腕，跑向了观看烟花秀的最佳地点。

满耳都是人们的欢笑，满眼都是涌动的人潮，乐园的华灯照亮了人们的脸庞，也在地上映着斑斓的光，你们拉着手、踩着光斑在喧闹的乐园中奔跑，仿佛是某个夏夜中盛大的逃亡。

你们终于抵达了观景点，抵达的一瞬间，璀璨的烟火升上了夜幕。

无数的尖啸与光芒伴着烟火绽放，前一刻还是漆黑一片的天空，当烟火绽放，一切的黑暗都被驱散，深黑的夜被点缀成五光十色的白昼，无穷的光明向着大地盖下，落进湖泊，落进欢呼的人群，也落进了姜安心的眼眸。

那双眼中有着的本是苍凉的寂寞，可当烟火盛放，那双眼睛也被点亮，所有的灰暗所有的孤单都在璀璨中变得遥远。

趁着姜安心被烟火吸引的时候，你从被包装得毫不起眼的盒子里拿出了为她买来的生日蛋糕，插上蜡烛点燃，捧到她的面前，为她唱起生日祝福歌。

烛火跃动在姜安心的眼中，她的眼眸莹莹的亮着，而后，她闭上眼睛许下愿望，吹熄了烛火。火焰在她眼中消失了，似乎永无穷尽的烟火也在夜空中远去，但某种热度却在姜安心眼中留存，令那双眼睛不再孤寂。

生日快乐，姜安心。

可心底有一个声音又悄悄补了一句：生日快乐，吴倩尹。

你递给她蛋糕刀，又在她切之前叫住了她，用附赠的叉子把蛋糕上的蓝莓一颗颗的仔细挑去：“可以了，剩下的都是你喜欢吃的了。”

姜安心有些惊讶，她一定是好奇你怎么知道她不爱吃蓝莓。

你看着她，洞悉了她的一切小心思，你说：“我以前记性很差，现在，我学会了把在乎之人的习惯与喜好看在眼里，记在心中。”

是的，因为用心，所以才会细心，因为细心，所以会观察她的一举一动。曾经你记性很差，总是记不住关于吴倩尹的一切。和她分手之后，你好像学会了如何去爱一个人，爱人的能力，是她赋予你的。

姜安心听完你的回答，不再询问，但脸上的笑容已胜过刚才盛大的烟火。

只是，那个笑容没有维持很久。离开的时候，你们路过了乐园里的过山车，她停了下来，望着那里，眼神里的光随着烟火散尽了，只留下了漆黑的夜空和散不去的硝烟味。

“想坐过山车吗？我陪你去。”你问她。

沉默良久，她回你：“我恐高，从不坐过山车。”

天空飘起了小雨，而后雨势逐渐变得滂沱。

“快走吧，下雨了。”她说。

蒙蒙的雾气跟着暴雨沸腾起来，你脱下外套挡在姜安心头顶，顶着倾盆暴雨和苍茫雾色，带着她跑向躲雨的地方，你们默契地抬足奔跑，脚步契合着对方的步伐。

地面上迅速的积满了雨水，你们奔跑的身影落在雨水中央，脚尖扬起时飞溅的水滴没入水中的倒影，把倒影打得细碎，细碎的倒影在涟漪中交融。

你们躲到了一处屋檐下，你让姜安心在那里等你，然后自己跑去买伞。暴雨之中方向难以辨别，你顶着暴雨直到浑身湿透才找到一家超市。

原本店里还有两把伞，足够你和姜安心一人一把。但你最终只买下了一把伞，回到了姜安心身边，撑起伞让她走进伞下。

下雨了，吴倩尹，有没有伞？应该，有人为她撑伞吧。

看见你浑身湿透的模样，姜安心有些自责：“林沐阳，其实，我们只需要等待暴雨过去就好了，这暴雨，一定会过去的，不是吗？”

是啊，暴雨会过去的吧。你看着她明媚的眼睛，问她：“不觉得这样很浪漫吗？”

她没有肯定，也没有拒绝。

就像当初吴倩尹刚跟你表白的时候，你的反应一样。

**任务：**

分享你们本次的约会，等待节目组的安排

（未经导演组通知，请勿开始下一环节）
